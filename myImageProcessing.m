function varargout = myImageProcessing(varargin)

if nargin==1
    image = varargin{1};
    [size_y, size_x] = size(image);
    x = 1:size_x;   x = x - mean(x);
    y = 1:size_y;   y = y - mean(y);
else
    x = varargin{1};
    y = varargin{2};
    size_x = length(x);
    size_y = length(y);
    image = varargin{3};
end

if isempty(image)
    varargout{1} = []; % x
    varargout{2} = []; % y
    varargout{3} = image;
    varargout{4} = NaN;
    varargout{5} = NaN;
    varargout{6} = NaN;
    varargout{7} = NaN;    
    return
end


[X,Y] = meshgrid(x,y);


% ax = 0.1; % mm
% ay = 0.1; % mm
% M = exp(-(((X-mean(x))/ax).^2 + ((Y-mean(y))/ay).^2));
% image = abs( ifftshift( ifft2( fft2(image) .* fft2(M) ) ) ) / sum(M(:));

try
    image = medfilt2(image);
    image = medfilt2(image);
%   image = medfilt2(image);
%   
catch
    [val, ind] = histc(image(:), linspace(0, max(image(:)), 200));
    
    sval = cumsum(val);
    sval = sval / max(sval);
    ind1 = find(sval>0.9999);
    
    %ind1 = [20:200];
    
    image(ismember(ind, ind1)) = 0;
end

bkg = mean(mean(image(1:10,:))) + mean(mean(image(end-10:end,:))) + ...
    mean(mean(image(:,1:10))) + mean(mean(image(:,end-10:end)));

%image = image - bkg;

%image(image < 0) = 0;

val = max(image(:));

% bkg
% val
% sum(image(:))
% sum(image(:)) / val
val4cog = 0.4; %0.7; %Use all the data above this value for the cog determination. Ideally: 0.4 for laser (non gaussian profile), 0.7 for protons.

ind = image > val4cog * val;
if sum(ind(:))>20 && val > 2*bkg
    x_c = sum(X(ind(:)).*image(ind(:))) / sum(image(ind(:)));
    y_c = sum(Y(ind(:)).*image(ind(:))) / sum(image(ind(:)));
else
    x_c = NaN;
    y_c = NaN;
end

image_noNaN = image;
%image_noNaN(isnan(image_noNaN )) = 0; %do we want to calculate also if
%there are NaNs?
energy = sum(image_noNaN(:));
intensity = max(image_noNaN(:));

varargout{1} = x;
varargout{2} = y;
varargout{3} = image;
varargout{4} = x_c;
varargout{5} = y_c;
varargout{6} = energy;
varargout{7} = intensity;

