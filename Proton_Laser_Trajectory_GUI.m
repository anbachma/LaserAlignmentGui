function varargout = Proton_Laser_Trajectory_GUI(varargin)
% PROTON_LASER_TRAJECTORY_GUI MATLAB code for Proton_Laser_Trajectory_GUI.fig
%      PROTON_LASER_TRAJECTORY_GUI, by itself, creates a new PROTON_LASER_TRAJECTORY_GUI or raises the existing
%      singleton*.
%
%      H = PROTON_LASER_TRAJECTORY_GUI returns the handle to a new PROTON_LASER_TRAJECTORY_GUI or the handle to
%      the existing singleton*.
%
%      PROTON_LASER_TRAJECTORY_GUI('CALLBACK',hObject,eventData, handles,...) calls the local
%      function named CALLBACK in PROTON_LASER_TRAJECTORY_GUI.M with the given input arguments.
%
%      PROTON_LASER_TRAJECTORY_GUI('Property','Value',...) creates a new PROTON_LASER_TRAJECTORY_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Proton_Laser_Trajectory_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Proton_Laser_Trajectory_GUI_OpeningFcn via varargin.
%5
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Proton_Laser_Trajectory_GUI

% Last Modified by GUIDE v2.5 11-Aug-2017 15:24:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Proton_Laser_Trajectory_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @Proton_Laser_Trajectory_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before Proton_Laser_Trajectory_GUI is made visible.
function Proton_Laser_Trajectory_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Proton_Laser_Trajectory_GUI (see VARARGIN)

% Choose default command line output for Proton_Laser_Trajectory_GUI
handles.output = hObject;

% Open figure with TX,Y plots
%handles.handles_figure_TXY = guidata(TXY_History);
handles.handles_figure_TXY = [];

% Update handles structure
guidata(hObject, handles);
% UIWAIT makes Proton_Laser_Trajectory_GUI wait for user response (see UIRESUME)
% uiwait(handles.fig_Main);

% Call my initialization and store Devices structure into handles
Devices = myInitialize(handles);

% Updates tables and figures, will store Devices into setappdata(...)
myUpdate(handles, Devices, 'all'); % if 'all' argument is ommitted, then Update only tab_Dynamic and Figures 

try
    matlabJapc.staticINCAify('SPS')
end

% --- Outputs from this function are returned to the command line.
function varargout = Proton_Laser_Trajectory_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% ************************************************************************
% ************   Begin of   MY INITIALIZE  *******************************
% ************************************************************************

function Devices = myInitialize(handles, varargin)

% ----------  Devices skeleton  ----------------------------
% Static part
Devices.GroupName = '';
Devices.Name = ''; % main device indentifier
Devices.Trigger = ''; % 'Extraction' or '10 Hz'
Devices.Position = ''; % '' / 'Entrance' / 'Exit'
Devices.Acquire = logical(1); % 0 / 1
Devices.DisplayLast = logical(1); % 0 / 1
Devices.DisplayMean = logical(1); % 0 / 1
Devices.DisplayBuffer = logical(1); % 0 / 1
Devices.Shape = '';
Devices.Color = '';
Devices.Buffer_Length = 1000; % length of the buffer
Devices.Z_coord = []; % m,  
Devices.X_origin = 0; % mm, where "ideal" trajectory goes
Devices.Y_origin = 0; % mm, where "ideal" trajectory goes
Devices.Scale_X = 1; % units / mm
Devices.Scale_Y = 1; % units / mm
Devices.Scale_Intensity = []; % units/(J/cm2) or 1 for BCT, scales intensity

% Dynamic part, scaled, this what goes to a table and/or figures
Devices.Buffer_Counter = 0; % buffer counter
Devices.X_mm_last = []; % mm, current value
Devices.Y_mm_last = []; % mm, current value
Devices.X_mm_mean = []; % mm, mean value
Devices.Y_mm_mean = []; % mm, mean value
Devices.Intensity_scaled_last = [];
Devices.Intensity_scaled_mean = [];
Devices.Energy_scaled_last = [];
Devices.Energy_scaled_mean = [];
% Devices.timeStamp_ns_buffer = [];

% Other scalar / nonscalar data
Devices.Image = [];
Devices.Size_X = [];
Devices.Size_Y = [];
Devices.X_range = [];
Devices.Y_range = [];

Devices.X_buffer = []; % mm, here buffer array is stored
Devices.Y_buffer = []; % mm, here buffer array is stored
Devices.Intensity_buffer = [];
Devices.Energy_buffer = [];

Devices.timeStamp_ns = [];
Devices.image_handle = [];
Devices.XY_line_handle_last = [];
Devices.XY_line_handle_mean = [];
Devices.XY_line_handle_buffer = [];

Devices.TX_line_handle = [];
Devices.TY_line_handle = [];
Devices.TIntensity_handle = [];
Devices.TEnergy_handle = [];


% -----------------------------------------------

n = 1;
Devices(n,1).GroupName = 'BCT';
Devices(n,1).Name = 'TT41.BCTF.412340';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = '';

n = n + 1;
Devices(n,1).GroupName = 'BPM';
Devices(n,1).Name = 'TT41.BPM.412311';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = '';
Devices(n,1).Z_coord = 785.196; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 's';
Devices(n,1).Color = 'b';

n = n + 1;
Devices(n,1).GroupName = 'BPM';
Devices(n,1).Name = 'TT41.BPM.412319';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = '';
Devices(n,1).Z_coord = 789.025; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 's';
Devices(n,1).Color = 'b';

n = n + 1;
Devices(n,1).GroupName = 'BPM';
Devices(n,1).Name = 'TT41.BPM.412339';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = '';
Devices(n,1).Z_coord = 799.371; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 's';
Devices(n,1).Color = 'b';

n = n + 1;
Devices(n,1).GroupName = 'BPM';
Devices(n,1).Name = 'TT41.BPM.412352';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Entrance';
Devices(n,1).Z_coord = 805.669; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 's';
Devices(n,1).Color = 'b';

n = n + 1;
Devices(n,1).GroupName = 'BPM';
Devices(n,1).Name = 'TT41.BPM.412425';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Exit';
Devices(n,1).Z_coord = 819.217; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 's';
Devices(n,1).Color = 'b';

n = n + 1;
Devices(n,1).GroupName = 'BTV';
Devices(n,1).Name = 'TT41.BTV.412353';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Entrance';
Devices(n,1).Z_coord = 806.103; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 'd';
Devices(n,1).Color = 'g';

n = n + 1;
Devices(n,1).GroupName = 'BTV';
Devices(n,1).Name = 'TT41.BTV.412353.LASER';
Devices(n,1).Trigger = '10 Hz'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Entrance';
Devices(n,1).Z_coord = 806.103; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = '+';
Devices(n,1).Color = 'g';

n = n + 1;
Devices(n,1).GroupName = 'VLC';
Devices(n,1).Name = 'BOVWA.03TT41.CAM3'; % Virtual CCD, plasma entrance
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Entrance';
Devices(n,1).Z_coord = 807.3505; % m,  
Devices(n,1).Scale_X = 169.9; % units / mm
Devices(n,1).Scale_Y = 169; % units / mm
Devices(n,1).Shape = 'o';
Devices(n,1).Color = 'r';

n = n + 1;
Devices(n,1).GroupName = 'VLC';
Devices(n,1).Name = 'BOVWA.03TT41.CAM3'; % Virtual CCD, plasma entrance
Devices(n,1).Trigger = '10 Hz'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Entrance';
Devices(n,1).Z_coord = 807.3505; % m,  
Devices(n,1).Scale_X = 34; % units / mm
Devices(n,1).Scale_Y = 33.3; % units / mm
Devices(n,1).Shape = '*';
Devices(n,1).Color = 'r';

n = n + 1;
Devices(n,1).GroupName = 'BTV';
Devices(n,1).Name = 'TT41.BTV.412426';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Exit';
Devices(n,1).Z_coord = 819.651; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 'd';
Devices(n,1).Color = 'g';

n = n + 1;
Devices(n,1).GroupName = 'BTV';
Devices(n,1).Name = 'TT41.BTV.412426.LASER';
Devices(n,1).Trigger = '10 Hz'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Exit';
Devices(n,1).Z_coord = 819.651; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = '+';
Devices(n,1).Color = 'g';

n = n + 1;
Devices(n,1).GroupName = 'VLC';
Devices(n,1).Name = 'BOVWA.05TT41.CAM5'; % Virtual CCD, plasma entrance
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Exit';
Devices(n,1).Z_coord = 817.6505; % m,  
Devices(n,1).Scale_X = 169.9; % units / mm
Devices(n,1).Scale_Y = 169; % units / mm
Devices(n,1).Shape = 'o';
Devices(n,1).Color = 'r';

n = n + 1;
Devices(n,1).GroupName = 'VLC';
Devices(n,1).Name = 'BOVWA.05TT41.CAM5'; % Virtual CCD, plasma entrance
Devices(n,1).Trigger = '10 Hz'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Exit';
Devices(n,1).Z_coord = 817.6505; % m,  
Devices(n,1).Scale_X = 34; % units / mm
Devices(n,1).Scale_Y = 33.3; % units / mm
Devices(n,1).Shape = '*';
Devices(n,1).Color = 'r';

n = n + 1;
Devices(n,1).GroupName = 'BTV';
Devices(n,1).Name = 'BOVWA.02TCC4.AWAKECAM02'; % pxi cam: btv 442 core cam
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Exit';
Devices(n,1).Z_coord = 827.7894; % m,
Devices(n,1).Scale_X = 25.7; % units / mm
Devices(n,1).Scale_Y = 24.8; % units / mm
Devices(n,1).Shape = 'o';
Devices(n,1).Color = 'r';

n = n + 1;
Devices(n,1).GroupName = 'BTV';
Devices(n,1).Name = 'BOVWA.02TCC4.AWAKECAM02'; % pxi cam: btv 442 core cam
Devices(n,1).Trigger = '10 Hz'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Exit';
Devices(n,1).Z_coord = 827.7894; % m, 
Devices(n,1).Scale_X = 5.1; % units / mm
Devices(n,1).Scale_Y = 4.9; % units / mm
Devices(n,1).Shape = '*';
Devices(n,1).Color = 'r';

n = n + 1;
Devices(n,1).GroupName = 'BPM 352 pred by 311&339';
Devices(n,1).Name = 'TT41.BPM.412352_pred_by311_339';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Entrance';
Devices(n,1).Z_coord = 805.669; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 's';
Devices(n,1).Color = 'b';

n = n + 1;
Devices(n,1).GroupName = 'BPM 425 pred by 311&339';
Devices(n,1).Name = 'TT41.BPM.412425_pred_by311_339';
Devices(n,1).Trigger = 'Extraction'; % 'Extraction' or '10 Hz'
Devices(n,1).Position = 'Exit';
Devices(n,1).Z_coord = 819.217; % m,  
Devices(n,1).Scale_X = 1; % units / mm
Devices(n,1).Scale_Y = 1; % units / mm
Devices(n,1).Shape = 's';
Devices(n,1).Color = 'b';

for n = 1:length(Devices)
    Devices(n).Acquire = logical(1); % 0 / 1
    Devices(n).DisplayLast = logical(0); % 0 / 1
    Devices(n).DisplayMean = logical(0); % 0 / 1
    Devices(n).DisplayBuffer = logical(0); % 0 / 1
    Devices(n).Buffer_Length = 1000; % length of the buffer
    Devices(n).X_origin = 0; % mm, where "ideal" trajectory goes
    Devices(n).Y_origin = 0; % mm, where "ideal" trajectory goes
    Devices(n).Scale_Intensity = 1; % units/(J/cm2)
    Devices(n).Buffer_Counter = 0; % buffer counter
end


% Devices.GroupName = '';
% Devices.Name = ''; % main device indentifier
% Devices.Trigger = ''; % 'Extraction' or '10 Hz'
% Devices.Acquire = logical(1); % 0 / 1
% Devices.DisplayLast = logical(1); % 0 / 1
% Devices.DisplayMean = logical(1); % 0 / 1
% Devices.DisplayBuffer = logical(1); % 0 / 1
% Devices.Shape = '';
% Devices.Color = '';
% Devices.Buffer_Length = 1000; % length of the buffer
% Devices.Z_coord = []; % m,  
% Devices.X_origin = 0; % mm, where "ideal" trajectory goes
% Devices.Y_origin = 0; % mm, where "ideal" trajectory goes
% Devices.Scale_X = 1; % units / mm
% Devices.Scale_Y = 1; % units / mm
% Devices.Scale_Intensity = []; % units/(J/cm2) or 1 for BCT, scales intensity

ind = find(strcmp({Devices.Position}, 'Entrance'));
names = cellstr([char({Devices(ind).Name}')  repmat(' - ', [length(ind) 1]) char({Devices(ind).Trigger}')]);
handles.pmnu_Entrance_Image.String = [{'(none)'}; names];

ind = find(strcmp({Devices.Position}, 'Exit'));
names = cellstr([char({Devices(ind).Name}')  repmat(' - ', [length(ind) 1]) char({Devices(ind).Trigger}')]);
handles.pmnu_Exit_Image.String = [{'(none)'}; names];

setappdata(handles.fig_Main, 'Version', 'version 0.2')

pi;




% ************************************************************************
% ************   End of   MY INITIALIZE  *********************************
% ************************************************************************
% ************   Begin of   MY UPDATE TABLE  *****************************
% ************************************************************************

function varargout = myUpdate(handles, Devices, varargin, val_double)

warning('off', 'MATLAB:legend:PlotEmpty')

Names = {Devices.Name}';

if nargin>2 % update also Static table
    
    % % ----------  Devices skeleton  ----------------------------
    % % Static part
    % Devices.GroupName = '';
    % Devices.Name = ''; % main device indentifier
    % Devices.Trigger = ''; % 'Extraction' or '10 Hz'
    % Devices.Position = 'Entrance' / 'Exit'   
    % Devices.Acquire = logical(1); % 0 / 1
    % Devices.DisplayLast = logical(1); % 0 / 1
    % Devices.DisplayMean = logical(1); % 0 / 1
    % Devices.DisplayBuffer = logical(1); % 0 / 1
    % Devices.Shape = '';
    % Devices.Color = '';
    % Devices.Buffer_Length = 1000; % length of the buffer
    % Devices.Z_coord = []; % m,  
    % Devices.X_origin = 0; % mm, where "ideal" trajectory goes
    % Devices.Y_origin = 0; % mm, where "ideal" trajectory goes
    % Devices.Scale_X = 1; % units / mm
    % Devices.Scale_Y = 1; % units / mm
    % Devices.Scale_Intensity = []; % units/(J/cm2) or 1 for BCT, scales intensity

    
    
    % Origin calculation of virt dev
    
    
    
    
    handles.tab_Devices_Static.ColumnEditable = logical(ones([1 17]));
    handles.tab_Devices_Static.ColumnFormat = [repmat({'char'},[1 4]), repmat({'logical'},[1 4]), {'char', 'char'}, repmat({'numeric'},[1 7])];
    handles.tab_Devices_Static.ColumnName = [{'Group', 'Device', 'Trigger', 'Position', ...
        'Acuire?', 'Last', 'Mean', 'Buff', 'Shape', 'Color', ...
        'Buf.length', 'Z, m', 'X_origin, mm', 'Y_origin, mm', ...
        'Scale_X, au/mm', 'Scale_Y, au/mm', 'Scale_Intens'}];
    handles.tab_Devices_Static.ColumnWidth = repmat({'auto'}, [1 17]);

    Devices_Cell = struct2cell(Devices)';
    handles.tab_Devices_Static.Data = Devices_Cell(:,1:17);
    
end

% Update Dynamic table
% ----------  Devices skeleton  ----------------------------
% Dynamic part, scaled, this what goes to a table and/or figures
% Devices.Buffer_Counter = 1; % buffer counter
% Devices.X_mm_last = []; % mm, current value
% Devices.Y_mm_last = []; % mm, current value
% Devices.X_mm_mean = []; % mm, mean value
% Devices.Y_mm_mean = []; % mm, mean value
% Devices.Intensity_scaled_last = [];
% Devices.Intensity_scaled_mean = [];
% Devices.Energy_scaled_last = [];
% Devices.Energy_scaled_mean = [];

for n = 1:length(Devices)
    data_length = min(length(Devices(n).X_buffer), Devices(n).Buffer_Counter);
    if data_length==0
        Devices(n).X_mm_last = [];
    else
        Devices(n).X_mm_last = Devices(n).X_buffer(data_length) / Devices(n).Scale_X -  Devices(n).X_origin ;
    end

    data_length = min(length(Devices(n).Y_buffer), Devices(n).Buffer_Counter);
    if data_length==0
        Devices(n).Y_mm_last = [];
    else
        Devices(n).Y_mm_last = Devices(n).Y_buffer(data_length) / Devices(n).Scale_Y -  Devices(n).Y_origin ;
    end
    
    data_length = min(length(Devices(n).X_buffer), Devices(n).Buffer_Length);
    if data_length==0
        Devices(n).X_mm_mean = [];
    else
        res = Devices(n).X_buffer(1:data_length);
        ind = ~isnan(res);
        Devices(n).X_mm_mean = sum(res(ind)) ./ sum(ind) / Devices(n).Scale_X -  Devices(n).X_origin ;
    end

    data_length = min(length(Devices(n).Y_buffer), Devices(n).Buffer_Length);
    if data_length==0
        Devices(n).Y_mm_mean = [];
    else
        res = Devices(n).Y_buffer(1:data_length);
        ind = ~isnan(res);
        Devices(n).Y_mm_mean = sum(res(ind)) ./ sum(ind) / Devices(n).Scale_Y -  Devices(n).Y_origin ;
    end
   
    data_length = min(length(Devices(n).Intensity_buffer), Devices(n).Buffer_Counter);
    if data_length==0
        Devices(n).Intensity_scaled_last = [];
    else
        Devices(n).Intensity_scaled_last = Devices(n).Intensity_buffer(data_length) / Devices(n).Scale_Intensity;
    end
    
    data_length = min(length(Devices(n).Intensity_buffer), Devices(n).Buffer_Length);
    if data_length==0
        Devices(n).Intensity_scaled_mean = [];
    else
        res = Devices(n).Intensity_buffer(1:data_length);
        ind = ~isnan(res);
        Devices(n).Intensity_scaled_mean = sum(res(ind)) ./ sum(ind) / Devices(n).Scale_Intensity;
    end
    
    data_length = min(length(Devices(n).Energy_buffer), Devices(n).Buffer_Counter);
    if data_length==0
        Devices(n).Energy_scaled_last = [];
    else
        Devices(n).Energy_scaled_last = Devices(n).Energy_buffer(data_length) / Devices(n).Scale_Intensity;
    end
    
    data_length = min(length(Devices(n).Energy_buffer), Devices(n).Buffer_Length);
    if data_length==0
        Devices(n).Energy_scaled_mean = [];
    else
        res = Devices(n).Energy_buffer(1:data_length);
        ind = ~isnan(res);
        Devices(n).Energy_scaled_mean = sum(res(ind)) ./ sum(ind) / Devices(n).Scale_Intensity;
    end

end

% Generate scaled virt dev


handles.tab_Devices_Dynamic.ColumnEditable = logical(zeros([1 9]));
handles.tab_Devices_Dynamic.ColumnFormat = repmat({'numeric'},[1 9]);
handles.tab_Devices_Dynamic.ColumnName = {'Buffer_counter', 'X, mm', 'Y, mm', 'X_mean, mm', 'Y_mean, mm', ...
    'Intens', 'Intens_mean', 'Energy', 'Energy_mean'};
handles.tab_Devices_Dynamic.ColumnWidth = repmat({'auto'}, [1 9]);

Devices_Cell = struct2cell(Devices)';
handles.tab_Devices_Dynamic.Data = Devices_Cell(:, 17 + [1:9]);

% handles.tab_Devices_Dynamic.ColumnEditable = logical(zeros([1 10]));
% handles.tab_Devices_Dynamic.ColumnFormat = repmat({'numeric'},[1 10]);
% handles.tab_Devices_Dynamic.ColumnName = {'Buffer_counter', 'X, mm', 'Y, mm', 'X_mean, mm', 'Y_mean, mm', ...
%     'Intens', 'Intens_mean', 'Energy', 'Energy_mean', 'timeStamp_ns'};
% handles.tab_Devices_Dynamic.ColumnWidth = repmat({'auto'}, [1 10]);
% 
% Devices_Cell = struct2cell(Devices)';
% handles.tab_Devices_Dynamic.Data = Devices_Cell(:, [18:26, 36]);

setappdata(handles.fig_Main, 'Devices', Devices);

axis_size = 6; %5; % mm, half size

% *****************************************************

% Update XY Figures

cla(handles.ax_Entrance_XY)
%grid(handles.ax_Entrance_XY, 'on')
xlim(handles.ax_Entrance_XY, axis_size*[-1 1])
ylim(handles.ax_Entrance_XY, axis_size*[-1 1])
colormap(handles.ax_Entrance_XY, jet(64));
colorbar(handles.ax_Entrance_XY)
% caxis(handles.ax_Entrance_XY, [0,4000]);

cla(handles.ax_Exit_XY)
%grid(handles.ax_Exit_XY, 'on')
xlim(handles.ax_Exit_XY, axis_size*[-1 1])
ylim(handles.ax_Exit_XY, axis_size*[-1 1])
colormap(handles.ax_Exit_XY, jet(64));
colorbar(handles.ax_Exit_XY)
% caxis(handles.ax_Exit_XY, [0,4000]);

% At first, draw an imagesc, if any

if handles.pmnu_Entrance_Image.Value > 1
    ind = find(strcmp({Devices.Position}, 'Entrance'));
    ind = ind(handles.pmnu_Entrance_Image.Value - 1);
    image = double(Devices(ind).Image);
    x = Devices(ind).X_range / Devices(ind).Scale_X - Devices(ind).X_origin; % in mm and shifed to origin
    y = Devices(ind).Y_range / Devices(ind).Scale_Y - Devices(ind).Y_origin; % in mm and shifed to origin
    imagesc(x, y, image, 'Parent', handles.ax_Entrance_XY) 
end

if handles.pmnu_Exit_Image.Value > 1
    ind = find(strcmp({Devices.Position}, 'Exit'));
    ind = ind(handles.pmnu_Exit_Image.Value - 1);
    image = double(Devices(ind).Image);
    x = Devices(ind).X_range / Devices(ind).Scale_X - Devices(ind).X_origin; % in mm and shifed to origin
    y = Devices(ind).Y_range / Devices(ind).Scale_Y - Devices(ind).Y_origin; % in mm and shifed to origin
    imagesc(x, y, image, 'Parent', handles.ax_Exit_XY)    
end

% Entrance figure
legend_str = [];
hold(handles.ax_Entrance_XY, 'on');
ind = find(strcmp({Devices.Position}, 'Entrance'));
for n = 1:length(ind)
    if Devices(ind(n)).DisplayLast && ~isempty(Devices(ind(n)).X_mm_last) && ~isempty(Devices(ind(n)).Y_mm_last)
        plot(handles.ax_Entrance_XY, Devices(ind(n)).X_mm_last, ...
            Devices(ind(n)).Y_mm_last, ...
            Devices(ind(n)).Shape, 'MarkerSize', 8, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', Devices(ind(n)).Color);
        legend_str = [legend_str, {[Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - last']}];
    end
    if Devices(ind(n)).DisplayMean && ~isempty(Devices(ind(n)).X_mm_mean) && ~isempty(Devices(ind(n)).Y_mm_mean)
        plot(handles.ax_Entrance_XY, Devices(ind(n)).X_mm_mean, ...
            Devices(ind(n)).Y_mm_mean, ...
            Devices(ind(n)).Shape, 'MarkerSize', 12, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', [0.5 0.5 0.5]);
        legend_str = [legend_str, {[Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - mean']}];
    end
    if Devices(ind(n)).DisplayBuffer && ~isempty(Devices(ind(n)).X_buffer) && ~isempty(Devices(ind(n)).Y_buffer)
        x = Devices(ind(n)).X_buffer(1:min(length(Devices(ind(n)).X_buffer), Devices(ind(n)).Buffer_Length)) / Devices(ind(n)).Scale_X - Devices(ind(n)).X_origin;
        y = Devices(ind(n)).Y_buffer(1:min(length(Devices(ind(n)).Y_buffer), Devices(ind(n)).Buffer_Length)) / Devices(ind(n)).Scale_Y - Devices(ind(n)).Y_origin;
        plot(handles.ax_Entrance_XY, x, y, Devices(ind(n)).Shape, 'MarkerSize', 4, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', 'none');
        legend_str = [legend_str, {[Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - buffer']}];
    end
end    

if ~isempty(legend_str)
    legend(handles.ax_Entrance_XY, legend_str);
else
    legend(handles.ax_Entrance_XY, 'off');
end

% Exit figure
legend_str = [];
hold(handles.ax_Exit_XY, 'on');
ind = find(strcmp({Devices.Position}, 'Exit'));
for n = 1:length(ind)
    if Devices(ind(n)).DisplayLast && ~isempty(Devices(ind(n)).X_mm_last) && ~isempty(Devices(ind(n)).Y_mm_last)
        plot(handles.ax_Exit_XY, Devices(ind(n)).X_mm_last, ...
            Devices(ind(n)).Y_mm_last, ...
            Devices(ind(n)).Shape, 'MarkerSize', 8, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', Devices(ind(n)).Color);
        legend_str = [legend_str, {[Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - last']}];
    end
    if Devices(ind(n)).DisplayMean && ~isempty(Devices(ind(n)).X_mm_mean) && ~isempty(Devices(ind(n)).Y_mm_mean)
        plot(handles.ax_Exit_XY, Devices(ind(n)).X_mm_mean, ...
            Devices(ind(n)).Y_mm_mean, ...
            Devices(ind(n)).Shape, 'MarkerSize', 12, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', [0.5 0.5 0.5]);
        legend_str = [legend_str, {[Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - mean']}];
    end
    if Devices(ind(n)).DisplayBuffer && ~isempty(Devices(ind(n)).X_buffer) && ~isempty(Devices(ind(n)).Y_buffer)
        x = Devices(ind(n)).X_buffer(1:min(length(Devices(ind(n)).X_buffer), Devices(ind(n)).Buffer_Length)) / Devices(ind(n)).Scale_X - Devices(ind(n)).X_origin;
        y = Devices(ind(n)).Y_buffer(1:min(length(Devices(ind(n)).Y_buffer), Devices(ind(n)).Buffer_Length)) / Devices(ind(n)).Scale_Y - Devices(ind(n)).Y_origin;
        plot(handles.ax_Exit_XY, x, y, Devices(ind(n)).Shape, 'MarkerSize', 4, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', 'none');
        legend_str = [legend_str, {[Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - buffer']}];
    end
end    


if ~isempty(legend_str)
    legend(handles.ax_Exit_XY, legend_str);
else
    legend(handles.ax_Exit_XY, 'off');
end

grid(handles.ax_Entrance_XY, 'on')
grid(handles.ax_Exit_XY, 'on')
xlabel(handles.ax_Entrance_XY, 'x, mm')
xlabel(handles.ax_Exit_XY, 'x, mm')
ylabel(handles.ax_Entrance_XY, 'y, mm')
ylabel(handles.ax_Exit_XY, 'y, mm')


% *****************************************************

% Update TX,Y Figures

try
    val = ishandle(handles.handles_figure_TXY.fig_TXY); % just to avoid new empty figure to appear
catch
    val = 0;
    %lasterr
end

if val
    cla(handles.handles_figure_TXY.ax_Entrance_TX)
    %grid(handles.ax_Entrance_XY, 'on')
    %xlim(handles.ax_Entrance_XY, axis_size*[-1 1])
    %ylim(handles.ax_Entrance_XY, axis_size*[-1 1])
    %colormap(handles.ax_Entrance_XY, jet(64));
    %colorbar(handles.ax_Entrance_XY)

    cla(handles.handles_figure_TXY.ax_Exit_TX)
    %grid(handles.ax_Exit_XY, 'on')
    %xlim(handles.ax_Exit_XY, axis_size*[-1 1])
    %ylim(handles.ax_Exit_XY, axis_size*[-1 1])
    %colormap(handles.ax_Exit_XY, jet(64));
    %colorbar(handles.ax_Exit_XY)

    cla(handles.handles_figure_TXY.ax_Entrance_TY)
    cla(handles.handles_figure_TXY.ax_Exit_TY)
    
    
    % Entrance figure in x 
    legend_str = [];
    hold(handles.handles_figure_TXY.ax_Entrance_TX, 'on');
    ind = find(strcmp({Devices.Position}, 'Entrance'));
%     ind = intersect(ind,val_double);
    
    for n = 1:length(ind)
        if  Devices(ind(n)).DisplayBuffer && ...
           ~isempty(Devices(ind(n)).timeStamp_ns) && ...
           ~isempty(Devices(ind(n)).X_buffer) 

    x = Devices(ind(n)).X_buffer(1:min(length(Devices(ind(n)).X_buffer), Devices(ind(n)).Buffer_Length)) ...
                / Devices(ind(n)).Scale_X - Devices(ind(n)).X_origin;
    t = Devices(ind(n)).timeStamp_ns(1:min(length(Devices(ind(n)).timeStamp_ns), Devices(ind(n)).Buffer_Length)); % ns
    t = (t - t(end))*1e-09/3600; % hours

    plot(handles.handles_figure_TXY.ax_Entrance_TX, t, x, ['-' Devices(ind(n)).Shape], ...
            'MarkerSize', 4, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', 'none');

    legend_str = [legend_str, {['X - ' Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - buffer']}];

        end
    end    

    if ~isempty(legend_str)
        legend(handles.handles_figure_TXY.ax_Entrance_TX, legend_str);
    else
        legend(handles.handles_figure_TXY.ax_Entrance_TX, 'off');
    end

        
    % Entrance figure in y 
    legend_str = [];
    hold(handles.handles_figure_TXY.ax_Entrance_TY, 'on');
    ind = find(strcmp({Devices.Position}, 'Entrance'));
    for n = 1:length(ind)
        if  Devices(ind(n)).DisplayBuffer && ...
           ~isempty(Devices(ind(n)).timeStamp_ns) && ...
           ~isempty(Devices(ind(n)).Y_buffer)

    y = Devices(ind(n)).Y_buffer(1:min(length(Devices(ind(n)).Y_buffer), Devices(ind(n)).Buffer_Length)) ...
                / Devices(ind(n)).Scale_Y - Devices(ind(n)).Y_origin;
    t = Devices(ind(n)).timeStamp_ns(1:min(length(Devices(ind(n)).timeStamp_ns), Devices(ind(n)).Buffer_Length)); % ns
    t = (t - t(end))*1e-09/3600; % hours

    plot(handles.handles_figure_TXY.ax_Entrance_TY, t, y, ['-' Devices(ind(n)).Shape], ...
            'MarkerSize', 4, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', 'none');

    legend_str = [legend_str, {['Y - ' Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - buffer']}];

        end
    end    

    if ~isempty(legend_str)
        legend(handles.handles_figure_TXY.ax_Entrance_TY, legend_str);
    else
        legend(handles.handles_figure_TXY.ax_Entrance_TY, 'off');
    end

    
    % Exit figure in x
    legend_str = [];
    hold(handles.handles_figure_TXY.ax_Exit_TX, 'on');
    ind = find(strcmp({Devices.Position}, 'Exit'));
    for n = 1:length(ind)
        if  Devices(ind(n)).DisplayBuffer && ...
           ~isempty(Devices(ind(n)).timeStamp_ns) && ...
           ~isempty(Devices(ind(n)).X_buffer) 

    x = Devices(ind(n)).X_buffer(1:min(length(Devices(ind(n)).X_buffer), Devices(ind(n)).Buffer_Length)) ...
                / Devices(ind(n)).Scale_X - Devices(ind(n)).X_origin;
    t = Devices(ind(n)).timeStamp_ns(1:min(length(Devices(ind(n)).timeStamp_ns), Devices(ind(n)).Buffer_Length)); % ns
    t = (t - t(end))*1e-09/3600; % hours

    plot(handles.handles_figure_TXY.ax_Exit_TX, t, x, ['-' Devices(ind(n)).Shape], ...
            'MarkerSize', 4, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', 'none');

    legend_str = [legend_str, {['X - ' Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - buffer']}];

        end
    end    

    if ~isempty(legend_str)
        legend(handles.handles_figure_TXY.ax_Exit_TX, legend_str);
    else
        legend(handles.handles_figure_TXY.ax_Exit_TX, 'off');
    end
    
    
   % Exit figure in y
    legend_str = [];
    hold(handles.handles_figure_TXY.ax_Exit_TY, 'on');
    ind = find(strcmp({Devices.Position}, 'Exit'));
    for n = 1:length(ind)
        if  Devices(ind(n)).DisplayBuffer && ...
           ~isempty(Devices(ind(n)).timeStamp_ns) && ...
           ~isempty(Devices(ind(n)).Y_buffer)

    y = Devices(ind(n)).Y_buffer(1:min(length(Devices(ind(n)).Y_buffer), Devices(ind(n)).Buffer_Length)) ...
                / Devices(ind(n)).Scale_Y - Devices(ind(n)).Y_origin;
    t = Devices(ind(n)).timeStamp_ns(1:min(length(Devices(ind(n)).timeStamp_ns), Devices(ind(n)).Buffer_Length)); % ns
    t = (t - t(end))*1e-09/3600; % hours

    plot(handles.handles_figure_TXY.ax_Exit_TY, t, y, ['-' Devices(ind(n)).Shape], ...
            'MarkerSize', 4, 'MarkerEdgeColor', Devices(ind(n)).Color, 'MarkerFaceColor', 'none');

    legend_str = [legend_str, {['Y - ' Devices(ind(n)).GroupName ' - ' Devices(ind(n)).Trigger ' - buffer']}];

        end
    end    

    if ~isempty(legend_str)
        legend(handles.handles_figure_TXY.ax_Exit_TY, legend_str);
    else
        legend(handles.handles_figure_TXY.ax_Exit_TY, 'off');
    end
    
    
    grid(handles.handles_figure_TXY.ax_Entrance_TX, 'on')
    grid(handles.handles_figure_TXY.ax_Entrance_TY, 'on')
    grid(handles.handles_figure_TXY.ax_Exit_TX, 'on')
    grid(handles.handles_figure_TXY.ax_Exit_TY, 'on')
    xlabel(handles.handles_figure_TXY.ax_Entrance_TX, 't, h')
    xlabel(handles.handles_figure_TXY.ax_Entrance_TY, 't, h')
    xlabel(handles.handles_figure_TXY.ax_Exit_TX, 't, h')
    xlabel(handles.handles_figure_TXY.ax_Exit_TY, 't, h')
    ylabel(handles.handles_figure_TXY.ax_Entrance_TX, 'x, mm')
    ylabel(handles.handles_figure_TXY.ax_Entrance_TY, 'y, mm')
    ylabel(handles.handles_figure_TXY.ax_Exit_TX, 'x, mm')
    ylabel(handles.handles_figure_TXY.ax_Exit_TY, 'y, mm')

    limTXY = 5;
    ylim(handles.handles_figure_TXY.ax_Entrance_TX, [-limTXY,limTXY])
    ylim(handles.handles_figure_TXY.ax_Entrance_TY, [-limTXY,limTXY])
    ylim(handles.handles_figure_TXY.ax_Exit_TX, [-limTXY,limTXY])
    ylim(handles.handles_figure_TXY.ax_Exit_TY, [-limTXY,limTXY])
    
end

if nargout>0
    varargout{1} = Devices;
end




% ************************************************************************
% ************   End of   MY UPDATE  *****************************
% ************************************************************************
% ************   Begin of GUI CALLBACKS **********************************
% ************************************************************************

% --- Executes on button press in tglbtn_StartStop_Extraction.
function tglbtn_StartStop_Extraction_Callback(hObject, eventdata, handles)
% hObject    handle to tglbtn_StartStop_Extraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tglbtn_StartStop_Extraction
if handles.tglbtn_StartStop_Extraction.Value % Pressed, was released before
   % handles.tab_Devices_Static.Enable = 'off'; % Disable table from being edited
   % handles.pbtn_Clear_Buffer.Enable = 'off';
   handles.tglbtn_StartStop_Extraction.Enable = 'off';
   handles.tglbtn_StartStop_Extraction.String = 'STOP Extraction';
   handles.tglbtn_StartStop_Extraction.FontWeight = 'bold';
   handles.tglbtn_StartStop_Extraction.ForegroundColor = [1 0 0]; % red
   %disp('Pressed')

   selector = 'SPS.USER.AWAKE1';
   myMonitor_Extraction = matlabJapcMonitor(selector, ... % JAPC selector
    {'TT41.BCTF.412340/Acquisition'; % BCT
     'BPM.AWAKE.TT41/Acquisition'; % all BPM's
     'TT41.BTV.412353/Image';
     'TT41.BTV.412426/Image';
     'BOVWA.03TT41.CAM3/ExtractionImage';
     'BOVWA.05TT41.CAM5/ExtractionImage';
     'BOVWA.02TCC4.AWAKECAM02/ExtractionImage';},... % List of signals to monitor
    @(data, monitorObject) Proton_Laser_Trajectory_GUI('my_JAPC_Callback_Extraction', data, monitorObject, handles),... % callback function
    'comment');

    % Enable "fast strategy"
    myMonitor_Extraction.useFastStrategy(true)

    % Set a timeout after which even if not all the signals are arrived (after the first one arriving), a "data delivery" is triggered.
    myMonitor_Extraction.setFastStrategyTimeOut(5000); % in this case 200 [ms]

    % If they didn't arrive, try to force to get signals from "OnChange" and "Constant" parameters (the definition depends on the Fesa Class you are acquiring...)
    myMonitor_Extraction.forceFastStrategyGetOnChangeAndConstantValues(true);

    % If you want to allow many update per Basic Period, since version 1.3.0 you need to force it:
    myMonitor_Extraction.allowManyUpdatesPerCycle(true);

    % start
    myMonitor_Extraction.start()
   
   handles.tglbtn_StartStop_Extraction.UserData = myMonitor_Extraction; 
   
   handles.tglbtn_StartStop_Extraction.Enable = 'on';
   
else % Released, was pressed before
   % handles.tab_Devices_Static.Enable = 'on'; % Enable table
   % handles.pbtn_Clear_Buffer.Enable = 'on';
   handles.tglbtn_StartStop_Extraction.Enable = 'off';
   handles.tglbtn_StartStop_Extraction.String = 'START Extraction'; 
   handles.tglbtn_StartStop_Extraction.FontWeight = 'normal';
   handles.tglbtn_StartStop_Extraction.ForegroundColor = [0 0 0]; % black
   %disp('Released')
   
   myMonitor_Extraction = handles.tglbtn_StartStop_Extraction.UserData; 
   myMonitor_Extraction.stop()
   myMonitor_Extraction.delete()
   
   handles.tglbtn_StartStop_Extraction.UserData = myMonitor_Extraction; 
   
   handles.tglbtn_StartStop_Extraction.Enable = 'on';

end

% --- Executes on button press in tglbtn_StartStop_Fast10Hz.
function tglbtn_StartStop_Fast10Hz_Callback(hObject, eventdata, handles)
% hObject    handle to tglbtn_StartStop_Fast10Hz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tglbtn_StartStop_Fast10Hz
if handles.tglbtn_StartStop_Fast10Hz.Value % Pressed, was released before
   % handles.tab_Devices_Static.Enable = 'off'; % Disable table from being edited
   % handles.pbtn_Clear_Buffer.Enable = 'off';
   handles.tglbtn_StartStop_Fast10Hz.Enable = 'off';
   handles.tglbtn_StartStop_Fast10Hz.String = 'STOP Fast10Hz';
   handles.tglbtn_StartStop_Fast10Hz.FontWeight = 'bold';
   handles.tglbtn_StartStop_Fast10Hz.ForegroundColor = [1 0 0]; % red
   %disp('Pressed')

%    selector = 'PULL.2000@SPS.USER.ALL';
%    myMonitor_Fast10Hz = matlabJapcMonitor(selector, ... % JAPC selector
%     {'SR.SCOPE27.CH01/Acquisition'},... % List of signals to monitor
%     @(data, monitorObject) Proton_Laser_Trajectory_GUI('my_JAPC_Callback_Fast10Hz', data, monitorObject, handles),... % callback function
%     'comment');
   
   %selector = 'SPS.USER.ALL';
   selector = 'PULL.1000@SPS.USER.AWAKE1';
   %selector = 'SPS.USER.AWAKE1';
   %'SR.SCOPE27.CH01/Acquisition'0.
   myMonitor_Fast10Hz = matlabJapcMonitor(selector, ... % JAPC selector
    {'TT41.BTV.412353.LASER/Image';
     'TT41.BTV.412426.LASER/Image';
     'BOVWA.03TT41.CAM3/CameraImage';
     'BOVWA.05TT41.CAM5/CameraImage';
     'BOVWA.02TCC4.AWAKECAM02/CameraImage'},... % List of signals to monitor
    @(data, monitorObject) Proton_Laser_Trajectory_GUI('my_JAPC_Callback_Fast10Hz', data, monitorObject, handles),... % callback function
    'comment');

    % Enable "fast strategy"
    myMonitor_Fast10Hz.useFastStrategy(true)

    % Set a timeout after which even if not all the signals are arrived (after the first one arriving), a "data delivery" is triggered.
    myMonitor_Fast10Hz.setFastStrategyTimeOut(800); % in this case 200 [ms]

    % If they didn't arrive, try to force to get signals from "OnChange" and "Constant" parameters (the definition depends on the Fesa Class you are acquiring...)
    myMonitor_Fast10Hz.forceFastStrategyGetOnChangeAndConstantValues(true);

    % If you want to allow many update per Basic Period, since version 1.3.0 you need to force it:
    myMonitor_Fast10Hz.allowManyUpdatesPerCycle(true);

    % start
    myMonitor_Fast10Hz.start()    
    
   handles.tglbtn_StartStop_Fast10Hz.UserData = myMonitor_Fast10Hz; 
   
   handles.tglbtn_StartStop_Fast10Hz.Enable = 'on';
   
else % Released, was pressed before
   % handles.tab_Devices_Static.Enable = 'on'; % Enable table
   % handles.pbtn_Clear_Buffer.Enable = 'on';
   handles.tglbtn_StartStop_Fast10Hz.Enable = 'off';
   handles.tglbtn_StartStop_Fast10Hz.String = 'START Fast10Hz'; 
   handles.tglbtn_StartStop_Fast10Hz.FontWeight = 'normal';
   handles.tglbtn_StartStop_Fast10Hz.ForegroundColor = [0 0 0]; % black
   %disp('Released')
   
   myMonitor_Fast10Hz = handles.tglbtn_StartStop_Fast10Hz.UserData; 
   myMonitor_Fast10Hz.stop()
   myMonitor_Fast10Hz.delete()
   
   handles.tglbtn_StartStop_Fast10Hz.UserData = myMonitor_Fast10Hz; 
   
   handles.tglbtn_StartStop_Fast10Hz.Enable = 'on';

end

% --- Executes during object deletion, before destroying properties.
function fig_Main_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to fig_Main (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
   myMonitor = handles.tglbtn_StartStop_Extraction.UserData; 
   myMonitor.stop()
   myMonitor.delete()
   handles.tglbtn_StartStop_Extraction.UserData = myMonitor; 
end
try
   myMonitor = handles.tglbtn_StartStop_Fast10Hz.UserData; 
   myMonitor.stop()
   myMonitor.delete()
   handles.tglbtn_StartStop_Fast10Hz.UserData = myMonitor;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function my_JAPC_Callback_Extraction(data, monitorObject, handles)
tic
now_timeStamp_s = posixtime(datetime('now','TimeZone','UTC')); % s 
now_timeStamp_ns = posixtime(datetime('now','TimeZone','UTC')) * 1e+09; % ns 
threshold_ns = 10 * 1e+09; % ns

handles = guidata(handles.fig_Main); % it is not necessary, but if handles have been updated ufter subscription
Devices = getappdata(handles.fig_Main, 'Devices');
Names = {Devices.Name}';
% DEBUG
handles.pbtn_Clear_Buffer.UserData = data;
% DEBUG

n = find(strcmp(Names, 'TT41.BCTF.412340'));
if Devices(n).Acquire && isstruct(data.TT41_BCTF_412340.Acquisition.value)
    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end
    Devices(n).Intensity_buffer(Devices(n).Buffer_Counter) =data.TT41_BCTF_412340.Acquisition.value.totalIntensityPreferred;
    Devices(n).Energy_buffer(Devices(n).Buffer_Counter) = data.TT41_BCTF_412340.Acquisition.value.totalIntensity_unitExponent;
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.TT41_BCTF_412340.Acquisition.timeStamp;
else
end

if isstruct(data.BPM_AWAKE_TT41.Acquisition.value)
    
    n = find(strcmp(Names, 'TT41.BPM.412311'));
    if Devices(n).Acquire
        ind = find(strcmp(data.BPM_AWAKE_TT41.Acquisition.value.bpmNames, 'TT41.BPM.412311'));
        Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
        if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end
        Devices(n).X_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.horPosition(ind);
        Devices(n).Y_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.verPosition(ind);
%         % DEBUG !!!
%         x1_fordebugging = 2*randn-1+2;
%         y1_fordebugging = 2*randn-1-3;
%         Devices(n).X_buffer(Devices(n).Buffer_Counter) = x1_fordebugging; % mm, here should go a beam position
%         Devices(n).Y_buffer(Devices(n).Buffer_Counter) = y1_fordebugging; % mm, here should go a beam position      
%         % DEBUG !!!
        
        Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.timeStamp;
    end
    
    n = find(strcmp(Names, 'TT41.BPM.412319'));
    if Devices(n).Acquire
        ind = find(strcmp(data.BPM_AWAKE_TT41.Acquisition.value.bpmNames, 'TT41.BPM.412319'));
        Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
        if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end
        Devices(n).X_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.horPosition(ind);
        Devices(n).Y_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.verPosition(ind);
%         % DEBUG !!!
%         x1_fordebugging = 2*randn-1+2;
%         y1_fordebugging = 2*randn-1-3;
%         Devices(n).X_buffer(Devices(n).Buffer_Counter) = x1_fordebugging; % mm, here should go a beam position
%         Devices(n).Y_buffer(Devices(n).Buffer_Counter) = y1_fordebugging; % mm, here should go a beam position      
%         % DEBUG !!!
        
        Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.timeStamp;
    end
    
    n = find(strcmp(Names, 'TT41.BPM.412339'));
    if Devices(n).Acquire
        ind = find(strcmp(data.BPM_AWAKE_TT41.Acquisition.value.bpmNames, 'TT41.BPM.412339'));
        Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
        if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end
        Devices(n).X_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.horPosition(ind);
        Devices(n).Y_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.verPosition(ind);
%         % DEBUG !!!        
%         x2_fordebugging = 2*randn-1+2;
%         y2_fordebugging = 2*randn-1-3;
%         Devices(n).X_buffer(Devices(n).Buffer_Counter) = x2_fordebugging; % mm, here should go a beam position
%         Devices(n).Y_buffer(Devices(n).Buffer_Counter) = y2_fordebugging; % mm, here should go a beam position
%         % DEBUG !!!
        Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.timeStamp;
    end
    
    n = find(strcmp(Names, 'TT41.BPM.412352'));
    if Devices(n).Acquire
        ind = find(strcmp(data.BPM_AWAKE_TT41.Acquisition.value.bpmNames, 'TT41.BPM.412352'));
        Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
        if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end
        Devices(n).X_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.horPosition(ind);
        Devices(n).Y_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.verPosition(ind);
%         % DEBUG !!!
%         Devices(n).X_buffer(Devices(n).Buffer_Counter) = 2*randn-1+2; % mm, here should go a beam position
%         Devices(n).Y_buffer(Devices(n).Buffer_Counter) = 2*randn-1-3; % mm, here should go a beam position
%         % DEBUG !!!
        Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.timeStamp;
    end        
    
    n = find(strcmp(Names, 'TT41.BPM.412425'));
    if Devices(n).Acquire
        ind = find(strcmp(data.BPM_AWAKE_TT41.Acquisition.value.bpmNames, 'TT41.BPM.412425'));
        Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
        if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end
        Devices(n).X_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.horPosition(ind);
        Devices(n).Y_buffer(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.value.verPosition(ind);
%         % DEBUG !!!
%         Devices(n).X_buffer(Devices(n).Buffer_Counter) = 2*randn-1; % mm, here should go a beam position
%         Devices(n).Y_buffer(Devices(n).Buffer_Counter) = 2*randn-1; % mm, here should go a beam position
%         % DEBUG !!!
        Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BPM_AWAKE_TT41.Acquisition.timeStamp;
    end
    
else
end

n = find(strcmp(Names, 'TT41.BTV.412353'));
if Devices(n).Acquire && isstruct(data.TT41_BTV_412353.Image.value)
    Devices(n).Image = double(data.TT41_BTV_412353.Image.value.imageSet);
    Devices(n).X_range = sort(data.TT41_BTV_412353.Image.value.imagePositionSet1); % mm, already scaled
    Devices(n).Y_range = sort(data.TT41_BTV_412353.Image.value.imagePositionSet2); % mm, already scaled    
    Devices(n).Size_X = length(Devices(n).X_range);
    Devices(n).Size_Y = length(Devices(n).Y_range);
    Devices(n).Image = reshape(Devices(n).Image, [Devices(n).Size_X Devices(n).Size_Y])';
    
    % Apply transformation to make it similar to CAM3 :   rot90
    % Matlab rot90 function rotates matrix COUNTERCLOCKWISE !!! 
    Devices(n).Image = rot90(Devices(n).Image, -1); % rotate clockwise
%     before 25.05.2018
    %Devices(n).Image = rot90(flipud(Devices(n).Image), -1); % rotate clockwise and flip up down since 25.05.2018

    [Devices(n).X_range, Devices(n).Y_range] = deal(Devices(n).Y_range, Devices(n).X_range);
    [Devices(n).Size_X, Devices(n).Size_Y] = deal(Devices(n).Size_Y, Devices(n).Size_X);
    % ************************************
        %crop image to avoid dark pixels at the edges (especially for the
        %background subtraction)
            x_beg = 0; %number of pixels to be cut at x beginning
            x_end = 0; %number of pixels to be cut at x end
            y_beg = 0; %number of pixels to be cut at y beginning
            y_end = 0; %number of pixels to be cut at y end
            Devices(n).Image(:,1:x_beg) = []; %cut at x beginning
            Devices(n).Image(:,size(Devices(n).Image,2)-x_end+1:size(Devices(n).Image,2)) = []; %cut at x end
            Devices(n).Image(1:y_beg,:) = []; %cut at y beginning
            Devices(n).Image(size(Devices(n).Image,1)-y_end+1:size(Devices(n).Image,1),:) = []; %cut at y end
            Devices(n).X_range(1:x_beg) = []; %cut at x beginning
            Devices(n).X_range(end-x_end+1:end) = []; %cut at x end
            Devices(n).Y_range(1:y_beg) = [];  %cut at y beginning
            Devices(n).Y_range(end-y_end+1:end) = []; %cut at y end
            Devices(n).Size_X = length(Devices(n).X_range);
            Devices(n).Size_Y = length(Devices(n).Y_range);    
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    

     [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   

     % DEBUG !!!
%     x1_fordebugging  = (2*randn-1);% mm, here should go a beam position
%     y1_fordebugging = (2*randn-1);
%     Devices(n).X_buffer(Devices(n).Buffer_Counter) = x1_fordebugging; % mm, here should go a beam position
%     Devices(n).Y_buffer(Devices(n).Buffer_Counter) = y1_fordebugging; % mm, here should go a beam position
    % DEBUG !!!

    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.TT41_BTV_412353.Image.timeStamp;
        
else
end

n = find(strcmp(Names, 'TT41.BTV.412426'));
if Devices(n).Acquire && isstruct(data.TT41_BTV_412426.Image.value)
    Devices(n).Image = double(data.TT41_BTV_412426.Image.value.imageSet);
    Devices(n).X_range = sort(data.TT41_BTV_412426.Image.value.imagePositionSet1); % mm, already scaled?
    Devices(n).Y_range = sort(data.TT41_BTV_412426.Image.value.imagePositionSet2); % mm, already scaled?
    Devices(n).Size_X = length(Devices(n).X_range);
    Devices(n).Size_Y = length(Devices(n).Y_range);
    Devices(n).Image = reshape(Devices(n).Image, [Devices(n).Size_X Devices(n).Size_Y])';
    
    % Apply transformation to make it similar to CAM3 :   rot90
    % Matlab rot90 function rotates matrix COUNTERCLOCKWISE !!! 
    Devices(n).Image = rot90(fliplr(Devices(n).Image), -1); % fliplr and rotate clockwise
   % Devices(n).Image = rot90(flipud(fliplr(Devices(n).Image)), -1); % fliplr flipud and rotate clockwise, from 25.05.2018

    [Devices(n).X_range, Devices(n).Y_range] = deal(Devices(n).Y_range, Devices(n).X_range);
    [Devices(n).Size_X, Devices(n).Size_Y] = deal(Devices(n).Size_Y, Devices(n).Size_X);
    % ************************************
        %crop image to avoid dark pixels at the edges (especially for the
        %background subtraction)
            x_beg = 0; %number of pixels to be cut at x beginning
            x_end = 5; %number of pixels to be cut at x end
            y_beg = 0; %number of pixels to be cut at y beginning
            y_end = 0; %number of pixels to be cut at y end
            Devices(n).Image(:,1:x_beg) = []; %cut at x beginning
            Devices(n).Image(:,size(Devices(n).Image,2)-x_end+1:size(Devices(n).Image,2)) = []; %cut at x end
            Devices(n).Image(1:y_beg,:) = []; %cut at y beginning
            Devices(n).Image(size(Devices(n).Image,1)-y_end+1:size(Devices(n).Image,1),:) = []; %cut at y end
            Devices(n).X_range(1:x_beg) = []; %cut at x beginning
            Devices(n).X_range(end-x_end+1:end) = []; %cut at x end
            Devices(n).Y_range(1:y_beg) = [];  %cut at y beginning
            Devices(n).Y_range(end-y_end+1:end) = []; %cut at y end
            Devices(n).Size_X = length(Devices(n).X_range);
            Devices(n).Size_Y = length(Devices(n).Y_range);
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    
    
     [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   
    

    % DEBUG !!!
%     x2_fordebugging  = (2*randn-1);% mm, here should go a beam position
%     y2_fordebugging = (2*randn-1);
%     Devices(n).X_buffer(Devices(n).Buffer_Counter) = x2_fordebugging; % mm, here should go a beam position
%     Devices(n).Y_buffer(Devices(n).Buffer_Counter) = y2_fordebugging; % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.TT41_BTV_412426.Image.timeStamp;
else
end

    
n = find(strcmp(Names, 'BOVWA.03TT41.CAM3') & strcmp({Devices.Trigger}', 'Extraction'));
if Devices(n).Acquire && isstruct(data.BOVWA_03TT41_CAM3.ExtractionImage.value)
    Devices(n).Image = double(data.BOVWA_03TT41_CAM3.ExtractionImage.value.imageRawData);
    Devices(n).Size_X = size(Devices(n).Image, 2);
    Devices(n).Size_Y = size(Devices(n).Image, 1);
    Devices(n).X_range = [1:Devices(n).Size_X];
    Devices(n).Y_range = [1:Devices(n).Size_Y];
    Devices(n).X_range = (Devices(n).X_range - mean(Devices(n).X_range)); % au
    Devices(n).Y_range = (Devices(n).Y_range - mean(Devices(n).Y_range)); % au
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    
    
    % call a function here to calculate beam X,Y
    % ...    
    
    [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   
    E_thresh_VLC = 3e7; 
    if Devices(n).Energy_buffer(Devices(n).Buffer_Counter) < E_thresh_VLC
        Devices(n).X_buffer(Devices(n).Buffer_Counter) = NaN;
        Devices(n).Y_buffer(Devices(n).Buffer_Counter) = NaN;
    end 
    % DEBUG !!!
    % Devices(n).X_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % Devices(n).Y_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BOVWA_03TT41_CAM3.ExtractionImage.timeStamp;

else
end

n = find(strcmp(Names, 'BOVWA.05TT41.CAM5') & strcmp({Devices.Trigger}', 'Extraction'));
if Devices(n).Acquire && isstruct(data.BOVWA_05TT41_CAM5.ExtractionImage.value)
    Devices(n).Image = double(data.BOVWA_05TT41_CAM5.ExtractionImage.value.imageRawData);
    Devices(n).Size_X = size(Devices(n).Image, 2);
    Devices(n).Size_Y = size(Devices(n).Image, 1);
    Devices(n).X_range = [1:Devices(n).Size_X];
    Devices(n).Y_range = [1:Devices(n).Size_Y];
    Devices(n).X_range = (Devices(n).X_range - mean(Devices(n).X_range)); % au
    Devices(n).Y_range = (Devices(n).Y_range - mean(Devices(n).Y_range)); % au
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    
    
    % call a function here to calculate beam X,Y
    % ...    
    
    [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   
    E_thresh_VLC = 3e7; 
    if Devices(n).Energy_buffer(Devices(n).Buffer_Counter) < E_thresh_VLC
        Devices(n).X_buffer(Devices(n).Buffer_Counter) = NaN;
        Devices(n).Y_buffer(Devices(n).Buffer_Counter) = NaN;
    end 
    
    % DEBUG !!!
    % Devices(n).X_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % Devices(n).Y_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BOVWA_05TT41_CAM5.ExtractionImage.timeStamp;

else
end

n = find(strcmp(Names, 'BOVWA.02TCC4.AWAKECAM02') & strcmp({Devices.Trigger}', 'Extraction'));
if Devices(n).Acquire && isstruct(data.BOVWA_02TCC4_AWAKECAM02.ExtractionImage.value)
    Devices(n).Image = double(data.BOVWA_02TCC4_AWAKECAM02.ExtractionImage.value.imageRawData);
    Devices(n).Size_X = size(Devices(n).Image, 2);
    Devices(n).Size_Y = size(Devices(n).Image, 1);
    
    Devices(n).X_range = [1:Devices(n).Size_X];
    Devices(n).Y_range = [1:Devices(n).Size_Y];
    Devices(n).X_range = (Devices(n).X_range - mean(Devices(n).X_range)); % au
    Devices(n).Y_range = (Devices(n).Y_range - mean(Devices(n).Y_range)); % au
    
    % Apply transformation to make it similar to CAM3 :   rot-90
    % Matlab rot90 function rotates matrix COUNTERCLOCKWISE !!! 
    Devices(n).Image = rot90(Devices(n).Image); % rotate counterclockwise
    [Devices(n).X_range, Devices(n).Y_range] = deal(Devices(n).Y_range, Devices(n).X_range);
    [Devices(n).Size_X, Devices(n).Size_Y] = deal(Devices(n).Size_Y, Devices(n).Size_X);
    % ************************************
 
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    
    
    % call a function here to calculate beam X,Y
    % ...    
    
    [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   

    
    % DEBUG !!!
    % Devices(n).X_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % Devices(n).Y_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BOVWA_02TCC4_AWAKECAM02.ExtractionImage.timeStamp;

else
end


%******************   Generate data for VIRTUAL DEVICES   **********************
Names_VD = {'TT41.BPM.412352_pred_by311_339','TT41.BPM.412311','TT41.BPM.412339'; 
    'TT41.BPM.412425_pred_by311_339','TT41.BPM.412311','TT41.BPM.412339'};

for i = 1:size(Names_VD,1)
    n = find(strcmp(Names, Names_VD{i,1}));
    n_pre1 = find(strcmp(Names, Names_VD{i,2}));
    n_pre2 = find(strcmp(Names, Names_VD{i,3}));
   if Devices(n).Acquire && Devices(n_pre1).Acquire && Devices(n_pre2).Acquire 
        if Devices(n_pre1).Buffer_Counter > 0 && Devices(n_pre2).Buffer_Counter > 0 
            
if now_timeStamp_ns - Devices(n_pre1).timeStamp_ns(Devices(n_pre1).Buffer_Counter) < threshold_ns ...
   && now_timeStamp_ns - Devices(n_pre2).timeStamp_ns(Devices(n_pre2).Buffer_Counter) < threshold_ns 
           
       Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
       if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end
            x1 = Devices(n_pre1).X_buffer(Devices(n_pre1).Buffer_Counter) / Devices(n_pre1).Scale_X - Devices(n_pre1).X_origin ;  
            y1 = Devices(n_pre1).Y_buffer(Devices(n_pre1).Buffer_Counter) / Devices(n_pre1).Scale_Y - Devices(n_pre1).Y_origin ;
            x2 = Devices(n_pre2).X_buffer(Devices(n_pre2).Buffer_Counter) / Devices(n_pre2).Scale_X - Devices(n_pre2).X_origin ;  
            y2 = Devices(n_pre2).Y_buffer(Devices(n_pre2).Buffer_Counter) / Devices(n_pre2).Scale_Y - Devices(n_pre2).Y_origin ;

            [x3, y3] = myPrediction(x1,y1,x2,y2,Devices(n_pre1,1).Z_coord,Devices(n_pre2,1).Z_coord,Devices(n,1).Z_coord);
            
            if isempty(x3) || isempty(y3), x3 = NaN; y3 = NaN; end
            
            Devices(n).X_buffer(Devices(n).Buffer_Counter) = x3;
            Devices(n).Y_buffer(Devices(n).Buffer_Counter) = y3;
            
            
Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = max(Devices(n_pre1).timeStamp_ns(Devices(n_pre1).Buffer_Counter), Devices(n_pre2).timeStamp_ns(Devices(n_pre2).Buffer_Counter));
           
end
        end
   end
end


Devices = myUpdate(handles, Devices); % it will also save Devices into setappdata(...)


% *************************
% push the variables to FESA
% *************************

trig = 'Extraction'; 
GroupNames = {Devices.GroupName}';
deci = 1e3; %x, y and x0 and y0 precision of a um

n = 1; 
dev = 'TT41.BCTF.412340'; 
field = 'timeStamp_ns';
names_to_push{n} = [dev ' ' field]; %BCT
val = Devices(find(strcmp(Names, dev))).(field);
        if ~isempty(val) 
            values_to_push(n) = val(end); %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev1 = 'BOVWA.03TT41.CAM3';
dev2 = 'TT41.BPM.412352_pred_by311_339';
names_to_push(n) = {['|' dev1 '-' dev2 '| r_mm']};   %difference entrance p+ laser extr, y
val = round(sqrt( (Devices(find(strcmp(Names, dev1) & strcmp({Devices.Trigger}', trig))).X_mm_last-Devices(find(strcmp(Names, dev2))).X_mm_last).^2 ...
    + (Devices(find(strcmp(Names, dev1) & strcmp({Devices.Trigger}', trig))).Y_mm_last-Devices(find(strcmp(Names, dev2))).Y_mm_last).^2 )*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end

n = n+1; 
dev1 = 'BOVWA.05TT41.CAM5';
dev2 = 'TT41.BPM.412425_pred_by311_339';
names_to_push(n) = {['|' dev1 '-' dev2 '| r_mm']};   %difference entrance p+ laser extr, y
val = round(sqrt( (Devices(find(strcmp(Names, dev1) & strcmp({Devices.Trigger}', trig))).X_mm_last-Devices(find(strcmp(Names, dev2))).X_mm_last).^2 ...
    + (Devices(find(strcmp(Names, dev1) & strcmp({Devices.Trigger}', trig))).Y_mm_last-Devices(find(strcmp(Names, dev2))).Y_mm_last).^2 )*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end

n = n+1; 
dev = 'BOVWA.03TT41.CAM3';
names_to_push(n) = {[dev ' r_mm']}; %VLC3 extr, r
val = round(sqrt(Devices(find(strcmp(Names, dev) & strcmp({Devices.Trigger}', trig))).X_mm_last.^2 + Devices(find(strcmp(Names, dev) & strcmp({Devices.Trigger}', trig))).Y_mm_last.^2)*deci)/deci; %VLC3 extr, r
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'BOVWA.05TT41.CAM5';
names_to_push(n) = {[dev ' r_mm']}; %VLC5 extr, r
val = round(sqrt(Devices(find(strcmp(Names, dev) & strcmp({Devices.Trigger}', trig))).X_mm_last.^2 + Devices(find(strcmp(Names, dev) & strcmp({Devices.Trigger}', trig))).Y_mm_last.^2)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'TT41.BPM.412352_pred_by311_339';
names_to_push(n) = {[dev ' r_mm']};  %pred4BPM352 extr, r
val = round(sqrt(Devices(find(strcmp(Names, dev))).X_mm_last.^2 + Devices(find(strcmp(Names, dev))).Y_mm_last.^2)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'TT41.BPM.412425_pred_by311_339';
names_to_push(n) = {[dev ' r_mm']};  %pred4BPM425 extr, r
val = round(sqrt(Devices(find(strcmp(Names, dev))).X_mm_last.^2 + Devices(find(strcmp(Names, dev))).Y_mm_last.^2)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
                
n = n+1; 
dev = 'BOVWA.03TT41.CAM3'; 
field = 'X_mm_last';
names_to_push(n) = {[dev ' x_mm']}; %VLC3 extr, x
val= round(Devices(find(strcmp(Names, dev) & strcmp({Devices.Trigger}', trig))).(field)*deci)/deci; %VLC3 extr, x
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'BOVWA.03TT41.CAM3';
field = 'Y_mm_last';
names_to_push(n) = {[dev ' y_mm']}; %VLC3 extr, y
val = round(Devices(find(strcmp(Names, dev) & strcmp({Devices.Trigger}', trig))).(field)*deci)/deci; %VLC3 extr, x
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'BOVWA.05TT41.CAM5'; 
field = 'X_mm_last';
names_to_push(n) = {[dev ' x_mm']}; %VLC5 extr, x
val = round(Devices(find(strcmp(Names, dev) & strcmp({Devices.Trigger}', trig))).(field)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'BOVWA.05TT41.CAM5';
field = 'Y_mm_last';
names_to_push(n) = {[dev ' y_mm']}; %VLC5 extr, y
val = round(Devices(find(strcmp(Names, dev) & strcmp({Devices.Trigger}', trig))).(field)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'TT41.BPM.412352_pred_by311_339'; 
field = 'X_mm_last';
names_to_push(n) = {[dev ' x_mm']}; %pred4BPM352 extr, x
val = round(Devices(find(strcmp(Names, dev))).(field)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'TT41.BPM.412352_pred_by311_339'; 
field = 'Y_mm_last';
names_to_push(n) = {[dev ' y_mm']}; %pred4BPM352 extr, y
val = round(Devices(find(strcmp(Names, dev))).(field)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'TT41.BPM.412425_pred_by311_339';
field = 'X_mm_last';
names_to_push(n) = {[dev ' x_mm']}; %pred4BPM425 extr, x
val= round(Devices(find(strcmp(Names, dev))).(field)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'TT41.BPM.412425_pred_by311_339';
field = 'Y_mm_last';
names_to_push(n) = {[dev ' y_mm']}; %pred4BPM425 extr, y
val = round(Devices(find(strcmp(Names, dev))).(field)*deci)/deci; 
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev1 = 'BOVWA.03TT41.CAM3';
dev2 = 'TT41.BPM.412352_pred_by311_339';
field = 'X_mm_last';
names_to_push(n) = {['|' dev1 '-' dev2 '| x_mm']};  %difference entrance p+ laser extr, x
val = round(abs( Devices(find(strcmp(Names, dev1) & strcmp({Devices.Trigger}', trig))).(field) - Devices(find(strcmp(Names, dev2))).(field))*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev1 = 'BOVWA.03TT41.CAM3';
dev2 = 'TT41.BPM.412352_pred_by311_339';
field = 'Y_mm_last';
names_to_push(n) = {['|' dev1 '-' dev2 '| y_mm']};   %difference entrance p+ laser extr, y
val = round(abs( Devices(find(strcmp(Names, dev1) & strcmp({Devices.Trigger}', trig))).(field) - Devices(find(strcmp(Names, dev2))).(field))*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev1 = 'BOVWA.05TT41.CAM5';
dev2 = 'TT41.BPM.412425_pred_by311_339';
field = 'X_mm_last';
names_to_push(n) = {['|' dev1 '-' dev2 '| x_mm']};  %difference entrance p+ laser extr, x
val = round(abs( Devices(find(strcmp(Names, dev1) & strcmp({Devices.Trigger}', trig))).(field) - Devices(find(strcmp(Names, dev2))).(field))*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev1 = 'BOVWA.05TT41.CAM5';
dev2 = 'TT41.BPM.412425_pred_by311_339';
field = 'Y_mm_last';
names_to_push(n) = {['|' dev1 '-' dev2 '| y_mm']};   %difference entrance p+ laser extr, y
val = round(abs( Devices(find(strcmp(Names, dev1) & strcmp({Devices.Trigger}', trig))).(field) - Devices(find(strcmp(Names, dev2))).(field))*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
n = n+1; 
dev = 'TT41.BCTF.412340'; 
field1 = 'Intensity_scaled_last';
field2 = 'Energy_scaled_last';
names_to_push{n} = [dev ' p+ intensity']; %BCT intensity
%val = Devices(find(strcmp(Names, dev))).(field1) * 10^Devices(find(strcmp(Names, dev))).(field2) ; %BCT
val = Devices(find(strcmp(Names, dev))).(field1); %BCT intensity
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        

GroupNames = {Devices.GroupName}';

devGroup = 'BPM'; 
str = Devices(find(strcmp(GroupNames, devGroup)));
for m = 1:length(str)
    if m == 1; %timestamp only for one of the bpms because they are all the same
        n = n+1;
        field = 'timeStamp_ns';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = str(m).(field);
        if ~isempty(val) 
            values_to_push(n) = val(end); %BCT
        else 
            values_to_push(n) = NaN;
        end
    end
        n = n+1; 
        field = 'X_mm_last';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
        n = n+1; 
        field = 'Y_mm_last';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
                  
        n = n+1; 
        field = 'X_origin';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
        n = n+1; 
        field = 'Y_origin';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end        
end

devGroup = 'BTV'; 
str = Devices(find(strcmp(GroupNames, devGroup) & strcmp({Devices.Trigger}', trig)));

for m = 1:length(str)
        n = n+1;
        field = 'timeStamp_ns';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = str(m).(field);
        if ~isempty(val) 
            values_to_push(n) = val(end); %BCT
        else 
            values_to_push(n) = NaN;
        end
                
        n = n+1; 
        field = 'X_mm_last';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
        n = n+1;
        field = 'Y_mm_last';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
        n = n+1; 
        field = 'X_origin';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end

        n = n+1; 
        field = 'Y_origin';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
        n = n+1; 
        field = 'Energy_scaled_last';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val =  str(m).(field);
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
                
        n = n+1; 
        field = 'Intensity_scaled_last';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val =  str(m).(field);
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
end

devGroup = 'VLC'; 
str = Devices(find(strcmp(GroupNames, devGroup) & strcmp({Devices.Trigger}', trig)));

for m = 1:length(str)
        n = n+1;
        field = 'timeStamp_ns';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = str(m).(field);
        if ~isempty(val) 
            values_to_push(n) = val(end); %BCT
        else 
            values_to_push(n) = NaN;
        end
                
        n = n+1; 
        field = 'X_origin';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = str(m).(field);
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
        n = n+1; 
        field = 'Y_origin';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = round(str(m).(field)*deci)/deci;
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
        n = n+1; 
        field = 'Energy_scaled_last';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = str(m).(field);
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
        
        n = n+1; 
        field = 'Intensity_scaled_last';
        names_to_push{n} = [str(m).Name ' ' field]; %timestamp bpms in ns
        val = str(m).(field);
        if ~isempty(val) 
            values_to_push(n) = val; %BCT
        else 
            values_to_push(n) = NaN;
        end
         
end
    n = n+1; 
    names_to_push{n} = ['Variable_acquisition_time_s']; %timestamp bpms in ns
    val = now_timeStamp_s;
    if ~isempty(val) 
        values_to_push(n) = val; %BCT
    else 
        values_to_push(n) = NaN;
    end     
        
%%%%save txt file additionally 
filename = ['PointingExtr_' datestr(now, 'yyyymmdd_hhMMss')];
fid = fopen(['../Pointing_data4FESA/' filename], 'w');
for n = 1: length(values_to_push)
    str = strjoin(strcat(names_to_push(n), ';', num2str(values_to_push(n))));
    fprintf(fid, '%s \n', str);
end
fclose(fid);
%%%%

FesaLength = 100;
names_to_push(length(names_to_push)+1:FesaLength) = {''};
values_to_push(length(values_to_push)+1:FesaLength) = 0;

matlabJapc.staticSetSignal('','TSG41.AWAKE-LASER-DATA/NameSettings#nameValue',names_to_push)
matlabJapc.staticSetSignal('','TSG41.AWAKE-LASER-DATA/ValueSettings#floatValue',values_to_push)


disp(['my callback_extraction, run-time : ' num2str(toc*1000, '%0.0f') ' ms'])



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function my_JAPC_Callback_Fast10Hz(data, monitorObject, handles)
%disp('my callback_fast10Hz')

% DEBUG !!!
% data = handles.pbtn_Clear_Buffer.UserData; % fetch the proper extraction data
% my_JAPC_Callback_Extraction(data, monitorObject, handles) % call Extraction callback
% DEBUG !!!

tic
handles = guidata(handles.fig_Main); % it is not necessary, but if handles hav been updated ufter subscription
Devices = getappdata(handles.fig_Main, 'Devices');
Names = {Devices.Name}';

n = find(strcmp(Names, 'TT41.BTV.412353.LASER'));
if Devices(n).Acquire && isstruct(data.TT41_BTV_412353_LASER.Image.value)
    Devices(n).Image = double(data.TT41_BTV_412353_LASER.Image.value.imageSet);
    Devices(n).X_range = sort(data.TT41_BTV_412353_LASER.Image.value.imagePositionSet1); % mm, already scaled
    Devices(n).Y_range = sort(data.TT41_BTV_412353_LASER.Image.value.imagePositionSet2); % mm, already scaled    
    Devices(n).X_range = Devices(n).X_range + 0; % mm, 24.05.
    Devices(n).Y_range = Devices(n).Y_range + 0;%0.107; % mm, 24.05.
    Devices(n).Size_X = length(Devices(n).X_range);
    Devices(n).Size_Y = length(Devices(n).Y_range);
    Devices(n).Image = reshape(Devices(n).Image, [Devices(n).Size_X Devices(n).Size_Y])'; 
    
    % Apply transformation to make it similar to CAM3 :   rot90
    % Matlab rot90 function rotates matrix COUNTERCLOCKWISE !!! 
  %  Devices(n).Image = rot90(flipud(Devices(n).Image), -1); % rotate clockwise and flip up down since 25.05.2018
   Devices(n).Image = rot90(Devices(n).Image, -1); % rotate clockwise
%     before 25.05.2018
    [Devices(n).X_range, Devices(n).Y_range] = deal(Devices(n).Y_range, Devices(n).X_range);
    [Devices(n).Size_X, Devices(n).Size_Y] = deal(Devices(n).Size_Y, Devices(n).Size_X);
    % ************************************
        %crop image to avoid dark pixels at the edges (especially for the
        %background subtraction)
            x_beg = 10; %number of pixels to be cut at x beginning
            x_end = 12; %number of pixels to be cut at x end
            y_beg = 5; %number of pixels to be cut at y beginning
            y_end = 0; %number of pixels to be cut at y end
            Devices(n).Image(:,1:x_beg) = []; %cut at x beginning
            Devices(n).Image(:,size(Devices(n).Image,2)-x_end+1:size(Devices(n).Image,2)) = []; %cut at x end
            Devices(n).Image(1:y_beg,:) = []; %cut at y beginning
            Devices(n).Image(size(Devices(n).Image,1)-y_end+1:size(Devices(n).Image,1),:) = []; %cut at y end
            Devices(n).X_range(1:x_beg) = []; %cut at x beginning
            Devices(n).X_range(end-x_end+1:end) = []; %cut at x end
            Devices(n).Y_range(1:y_beg) = [];  %cut at y beginning
            Devices(n).Y_range(end-y_end+1:end) = []; %cut at y end
            Devices(n).Size_X = length(Devices(n).X_range);
            Devices(n).Size_Y = length(Devices(n).Y_range);
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    

     [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   

    % DEBUG !!!
    % Devices(n).X_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % Devices(n).Y_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.TT41_BTV_412353_LASER.Image.timeStamp;

else
end

n = find(strcmp(Names, 'TT41.BTV.412426.LASER'));
if Devices(n).Acquire && isstruct(data.TT41_BTV_412426_LASER.Image.value)
    Devices(n).Image = double(data.TT41_BTV_412426_LASER.Image.value.imageSet);
    Devices(n).X_range = sort(data.TT41_BTV_412426_LASER.Image.value.imagePositionSet1); % mm, already scaled?
    Devices(n).Y_range = sort(data.TT41_BTV_412426_LASER.Image.value.imagePositionSet2); % mm, already scaled?
%     Devices(n).X_range = Devices(n).X_range + 1.83; % mm, before
%     20.05.2018
    Devices(n).X_range = Devices(n).X_range - 0.63; % mm, 24.05.
    Devices(n).Y_range = Devices(n).Y_range - 1.37; % mm, 24.05.
    Devices(n).Size_X = length(Devices(n).X_range);
    Devices(n).Size_Y = length(Devices(n).Y_range);
    Devices(n).Image = reshape(Devices(n).Image, [Devices(n).Size_X Devices(n).Size_Y])';

    % Apply transformation to make it similar to CAM3 :   rot90
    % Matlab rot90 function rotates matrix COUNTERCLOCKWISE !!! 
      Devices(n).Image = rot90(fliplr(Devices(n).Image), -1); % fliplr and rotate clockwise
    %Devices(n).Image = rot90(flipud(fliplr(Devices(n).Image)), -1); % fliplr flipud and rotate clockwise, from 25.05.2018
    [Devices(n).X_range, Devices(n).Y_range] = deal(Devices(n).Y_range, Devices(n).X_range);
    [Devices(n).Size_X, Devices(n).Size_Y] = deal(Devices(n).Size_Y, Devices(n).Size_X);
    % ************************************
        %crop image to avoid dark pixels at the edges (especially for the
        %background subtraction)
            x_beg = 10; %number of pixels to be cut at x beginning
            x_end = 50; %number of pixels to be cut at x end
            y_beg = 0; %number of pixels to be cut at y beginning
            y_end = 30; %number of pixels to be cut at y end
            Devices(n).Image(:,1:x_beg) = []; %cut at x beginning
            Devices(n).Image(:,size(Devices(n).Image,2)-x_end+1:size(Devices(n).Image,2)) = []; %cut at x end
            Devices(n).Image(1:y_beg,:) = []; %cut at y beginning
            Devices(n).Image(size(Devices(n).Image,1)-y_end+1:size(Devices(n).Image,1),:) = []; %cut at y end
            Devices(n).X_range(1:x_beg) = []; %cut at x beginning
            Devices(n).X_range(end-x_end+1:end) = []; %cut at x end
            Devices(n).Y_range(1:y_beg) = [];  %cut at y beginning
            Devices(n).Y_range(end-y_end+1:end) = []; %cut at y end
            Devices(n).Size_X = length(Devices(n).X_range);
            Devices(n).Size_Y = length(Devices(n).Y_range);
            
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    
    
    [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   
    
    % DEBUG !!!
    % Devices(n).X_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % Devices(n).Y_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.TT41_BTV_412426_LASER.Image.timeStamp;
    
else
end

n = find(strcmp(Names, 'BOVWA.03TT41.CAM3') & strcmp({Devices.Trigger}', '10 Hz'));
if Devices(n).Acquire && isstruct(data.BOVWA_03TT41_CAM3.CameraImage.value)
    Devices(n).Image = double(data.BOVWA_03TT41_CAM3.CameraImage.value.image);
    Devices(n).Size_X = size(Devices(n).Image, 2);
    Devices(n).Size_Y = size(Devices(n).Image, 1);
    Devices(n).X_range = [1:Devices(n).Size_X];
    Devices(n).Y_range = [1:Devices(n).Size_Y];
    Devices(n).X_range = (Devices(n).X_range - mean(Devices(n).X_range)); % au
    Devices(n).Y_range = (Devices(n).Y_range - mean(Devices(n).Y_range)); % au
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    
    
    % call a function here to calculate beam X,Y
    % ...    
    
    [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   
     
    % DEBUG !!!
    % Devices(n).X_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % Devices(n).Y_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BOVWA_03TT41_CAM3.CameraImage.timeStamp;

else
end

n = find(strcmp(Names, 'BOVWA.05TT41.CAM5') & strcmp({Devices.Trigger}', '10 Hz'));
if Devices(n).Acquire && isstruct(data.BOVWA_05TT41_CAM5.CameraImage.value)
    Devices(n).Image = double(data.BOVWA_05TT41_CAM5.CameraImage.value.image);
    Devices(n).Size_X = size(Devices(n).Image, 2);
    Devices(n).Size_Y = size(Devices(n).Image, 1);
    Devices(n).X_range = [1:Devices(n).Size_X];
    Devices(n).Y_range = [1:Devices(n).Size_Y];
    Devices(n).X_range = (Devices(n).X_range - mean(Devices(n).X_range)); % au
    Devices(n).Y_range = (Devices(n).Y_range - mean(Devices(n).Y_range)); % au
        
          % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    
    
    % call a function here to calculate beam X,Y
    % ... 
    
    [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   
    
    % DEBUG !!!
    % Devices(n).X_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % Devices(n).Y_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BOVWA_05TT41_CAM5.CameraImage.timeStamp;

else
end

n = find(strcmp(Names, 'BOVWA.02TCC4.AWAKECAM02') & strcmp({Devices.Trigger}', '10 Hz'));
if Devices(n).Acquire && isstruct(data.BOVWA_02TCC4_AWAKECAM02.CameraImage.value)
    Devices(n).Image = double(data.BOVWA_02TCC4_AWAKECAM02.CameraImage.value.image);
    Devices(n).Size_X = size(Devices(n).Image, 2);
    Devices(n).Size_Y = size(Devices(n).Image, 1);
    Devices(n).X_range = [1:Devices(n).Size_X];
    Devices(n).Y_range = [1:Devices(n).Size_Y];
    Devices(n).X_range = (Devices(n).X_range - mean(Devices(n).X_range)); % au
    Devices(n).Y_range = (Devices(n).Y_range - mean(Devices(n).Y_range)); % au

    % Apply transformation to make it similar to CAM3 :   rot-90
    % Matlab rot90 function rotates matrix COUNTERCLOCKWISE !!! 
    Devices(n).Image = rot90(Devices(n).Image); % rotate counterclockwise
    [Devices(n).X_range, Devices(n).Y_range] = deal(Devices(n).Y_range, Devices(n).X_range);
    [Devices(n).Size_X, Devices(n).Size_Y] = deal(Devices(n).Size_Y, Devices(n).Size_X);
    % ************************************
 
    % DEBUG !!!
    % Devices(n).Image = Devices(n).Image + 50*(2*randn(size(Devices(n).Image))-1);
    % DEBUG !!!    

    Devices(n).Buffer_Counter = Devices(n).Buffer_Counter + 1;
    if Devices(n).Buffer_Counter > Devices(n).Buffer_Length, Devices(n).Buffer_Counter = 1; end    
    
    % call a function here to calculate beam X,Y
    % ... 
    
    [x, y, Devices(n).Image, Devices(n).X_buffer(Devices(n).Buffer_Counter), Devices(n).Y_buffer(Devices(n).Buffer_Counter), Devices(n).Energy_buffer(Devices(n).Buffer_Counter), Devices(n).Intensity_buffer(Devices(n).Buffer_Counter)] = ...
        myImageProcessing(Devices(n).X_range, Devices(n).Y_range, Devices(n).Image);   
    
    % DEBUG !!!
    % Devices(n).X_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % Devices(n).Y_buffer(Devices(n).Buffer_Counter) = (2*randn-1); % mm, here should go a beam position
    % DEBUG !!!
    
    Devices(n).timeStamp_ns(Devices(n).Buffer_Counter) = data.BOVWA_02TCC4_AWAKECAM02.CameraImage.timeStamp;

else
end
myUpdate(handles, Devices); % it will also save Devices into setappdata(...)

disp(['my callback_extraction, run-time : ' num2str(toc*1000, '%0.0f') ' ms'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on selection change in pmnu_Entrance_Image.
function pmnu_Entrance_Image_Callback(hObject, eventdata, handles)
% hObject    handle to pmnu_Entrance_Image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pmnu_Entrance_Image contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pmnu_Entrance_Image
myUpdate(handles, getappdata(handles.fig_Main, 'Devices')); % it will also save Devices into setappdata(...)

% --- Executes during object creation, after setting all properties.
function pmnu_Entrance_Image_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pmnu_Entrance_Image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pmnu_Exit_Image.
function pmnu_Exit_Image_Callback(hObject, eventdata, handles)
% hObject    handle to pmnu_Exit_Image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pmnu_Exit_Image contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pmnu_Exit_Image
myUpdate(handles, getappdata(handles.fig_Main, 'Devices')); % it will also save Devices into setappdata(...)

% --- Executes during object creation, after setting all properties.
function pmnu_Exit_Image_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pmnu_Exit_Image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when entered data in editable cell(s) in tab_Devices_Static.
function tab_Devices_Static_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tab_Devices_Static (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

Devices =  getappdata(handles.fig_Main, 'Devices');
FieldNames = fieldnames(Devices);

%makes sure that the origin and the scale of the virtual devices and the z coordinate cannot be
%changed
if ((    (eventdata.Indices(2) == find(strcmp(FieldNames, 'X_origin')) || (eventdata.Indices(2) == find(strcmp(FieldNames, 'Y_origin'))) || (eventdata.Indices(2) == find(strcmp(FieldNames, 'Scale_X')))    || (eventdata.Indices(2) == find(strcmp(FieldNames, 'Scale_Y')))  )  &&   (eventdata.Indices(1) > (size(Devices,1)-2)) )) || (eventdata.Indices(2) == find(strcmp(FieldNames, 'Z_coord')))    
    Devices(eventdata.Indices(1)).(FieldNames{eventdata.Indices(2)}) = eventdata.PreviousData;
else
    Devices(eventdata.Indices(1)).(FieldNames{eventdata.Indices(2)}) = eventdata.NewData;
end
    

myUpdate(handles, Devices, 'all'); % it will also save Devices into setappdata(...)

% --- Executes on button press in pbtn_Show_History.
function pbtn_Show_History_Callback(hObject, eventdata, handles)
% hObject    handle to pbtn_Show_History (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    figure(handles.handles_figure_TXY.fig_TXY); % bring to front
    drawnow
catch
    handles.handles_figure_TXY = guidata(TXY_History);
    guidata(handles.fig_Main, handles); % save modified handles structure
    %Devices =  getappdata(handles.fig_Main, 'Devices');
    myUpdate(handles, getappdata(handles.fig_Main, 'Devices'));
end

% --- Executes on button press in pbtn_Clear_Buffer.
function pbtn_Clear_Buffer_Callback(hObject, eventdata, handles)
% hObject    handle to pbtn_Clear_Buffer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

choice = questdlg('Which buffers you want to clear?','Clear buffer choice','Active buffers','All buffers','Cancel','Cancel');
if  strcmp(choice, 'Cancel')
    return
end

Devices =  getappdata(handles.fig_Main, 'Devices');
for n = 1:length(Devices)
    if strcmp(choice, 'All buffers') || (strcmp(choice, 'Active buffers') && Devices(n).Acquire)
    
        Devices(n).Buffer_Counter = 0;

        Devices(n).Image = [];
        Devices(n).Size_X = [];
        Devices(n).Size_Y = [];
        Devices(n).X_Range = [];
        Devices(n).Y_Range = [];  

        Devices(n).X_buffer = [];
        Devices(n).Y_buffer = [];
        Devices(n).Intensity_buffer = [];
        Devices(n).Energy_buffer = [];
        Devices(n).timeStamp_ns = [];
    
    end
end
myUpdate(handles, Devices)

% --------------------------------------------------------------------
function uitool_OpenFile_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uitool_OpenFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% uiopen('*.mat'); % this loads all variables to a Workspace
[file, path] = uigetfile('*.mat', 'Open Devices structure from a file:');
if ischar(file)
    res = load([path file], 'Devices','Version'); % tries to load Devices structure
    if isfield(res, 'Devices')
        myUpdate(handles, res.Devices, 'all')
    else
        msgbox({'File you selected does not contain Devices structure'; 'Choose another file'})
    end
    if isfield(res, 'Version')
        setappdata(handles.fig_Main, 'Version', res.Version);
    else
        msgbox({'Version is unknown'})
    end
    
end

% --------------------------------------------------------------------
function uitool_SaveFile_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uitool_SaveFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Devices = getappdata(handles.fig_Main, 'Devices');
Version = getappdata(handles.fig_Main, 'Version');
%uisave('Devices')
[file, path] = uiputfile('*.mat', 'Save Devices structure to a file:');
if ischar(file)
    save([path file], 'Devices', 'Version')
end



function ebox_Entrance_X_min_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Entrance_X_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Entrance_X_min as text
%        str2double(get(hObject,'String')) returns contents of ebox_Entrance_X_min as a double


% --- Executes during object creation, after setting all properties.
function ebox_Entrance_X_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Entrance_X_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ebox_Entrance_X_max_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Entrance_X_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Entrance_X_max as text
%        str2double(get(hObject,'String')) returns contents of ebox_Entrance_X_max as a double


% --- Executes during object creation, after setting all properties.
function ebox_Entrance_X_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Entrance_X_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ebox_Entrance_Y_min_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Entrance_Y_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Entrance_Y_min as text
%        str2double(get(hObject,'String')) returns contents of ebox_Entrance_Y_min as a double


% --- Executes during object creation, after setting all properties.
function ebox_Entrance_Y_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Entrance_Y_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ebox_Entrance_Y_max_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Entrance_Y_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Entrance_Y_max as text
%        str2double(get(hObject,'String')) returns contents of ebox_Entrance_Y_max as a double


% --- Executes during object creation, after setting all properties.
function ebox_Entrance_Y_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Entrance_Y_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ebox_Exit_X_min_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Exit_X_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Exit_X_min as text
%        str2double(get(hObject,'String')) returns contents of ebox_Exit_X_min as a double


% --- Executes during object creation, after setting all properties.
function ebox_Exit_X_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Exit_X_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ebox_Exit_Y_min_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Exit_Y_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Exit_Y_min as text
%        str2double(get(hObject,'String')) returns contents of ebox_Exit_Y_min as a double


% --- Executes during object creation, after setting all properties.
function ebox_Exit_Y_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Exit_Y_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ebox_Exit_Y_max_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Exit_Y_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Exit_Y_max as text
%        str2double(get(hObject,'String')) returns contents of ebox_Exit_Y_max as a double


% --- Executes during object creation, after setting all properties.
function ebox_Exit_Y_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Exit_Y_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ebox_Exit_X_max_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Exit_X_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Exit_X_max as text
%        str2double(get(hObject,'String')) returns contents of ebox_Exit_X_max as a double


% --- Executes during object creation, after setting all properties.
function ebox_Exit_X_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Exit_X_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in tglbtn_TakeReference.
function tglbtn_TakeReference_Callback(hObject, eventdata, handles)
% hObject    handle to tglbtn_TakeReference (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of tglbtn_StartStop_Reference_Callback

if handles.tglbtn_TakeReference.Value
    choice = questdlg({'It will clear the buffers of all active devices!'; 'At least some aquisition should be running for meaningful results'},'Referencing','OK','Cancel','Cancel');
    if  strcmp(choice, 'Cancel')
        handles.tglbtn_TakeReference.Value = 0;
        return
    end    
end

Devices =  getappdata(handles.fig_Main, 'Devices');

if handles.tglbtn_TakeReference.Value % Pressed, was released before
    
   handles.tglbtn_TakeReference.Enable = 'off';
   handles.tglbtn_TakeReference.String = 'STOP Referencing...';
   handles.tglbtn_TakeReference.FontWeight = 'bold';
   handles.tglbtn_TakeReference.ForegroundColor = [1 0 0]; % red

%
    n = [Devices.Acquire];

    [Devices(n).Buffer_Counter] = deal(0);

    [Devices(n).Image] = deal([]);
    [Devices(n).Size_X] = deal([]);
    [Devices(n).Size_Y] = deal([]);
    [Devices(n).X_Range] = deal([]);
    [Devices(n).Y_Range] = deal([]);  

    [Devices(n).X_buffer] = deal([]);
    [Devices(n).Y_buffer] = deal([]);
    [Devices(n).Intensity_buffer] = deal([]);
    [Devices(n).Energy_buffer] = deal([]);
    [Devices(n).timeStamp_ns] = deal([]);
%

   handles.tglbtn_TakeReference.Enable = 'on';


%    for n = 1:length(Devices)
%        if Devices(n).Acquire && n<14 && n>1 %excludes BCT (otherwise no values instead of 0) and virtual devices (only calculated and no offset)
%             Devices(n).X_origin = Devices(n).X_mean; % mm, reference trajectory defines origin
%             Devices(n).Y_origin = Devices(n).X_mean; % mm, reference trajectory defines origin
%        end
%    end
%    myUpdate(handles, Devices, 'all'); % it will also save Devices into setappdata(...)
   
else % Released, was pressed before
    
   handles.tglbtn_TakeReference.Enable = 'off';
   handles.tglbtn_TakeReference.String = 'Take Reference'; 
   handles.tglbtn_TakeReference.FontWeight = 'normal';
   handles.tglbtn_TakeReference.ForegroundColor = [0 0 0]; % black

%
    n = [Devices.Acquire];
    n(1) = 0; %excludes BCT (otherwise no values instead of 0)
    n(end-1:end) = 0; %excludes virtual devices (only calculated and no offset)
    [Devices(n).X_origin] = Devices(n).X_mm_mean; % mm, reference trajectory defines origin
    [Devices(n).Y_origin] = Devices(n).Y_mm_mean; % mm, reference trajectory defines origin  
    for i = 1:length(n)
    if (isempty(Devices(i).X_mm_mean) || isnan(Devices(i).X_mm_mean)) && Devices(i).Acquire
        [Devices(i).X_origin] = 0;
    end
    if (isempty(Devices(i).Y_mm_mean) || isnan(Devices(i).Y_mm_mean)) && Devices(i).Acquire
        [Devices(i).Y_origin] = 0;
    end
    end
handles.tglbtn_TakeReference.Enable = 'on';

end
%setappdata(handles.fig_Main, 'Devices', Devices);
myUpdate(handles, Devices, 'all')

