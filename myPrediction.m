function varargout = myPrediction(varargin)

if nargin==7
    x1 = varargin{1};
    y1 = varargin{2};
    x2 = varargin{3};
    y2 = varargin{4};
    z1 = varargin{5};
    z2 = varargin{6};
    z3 = varargin{7};
else
    msg = 'wrong number of inputs for myPrediction';
    error(msg)
end
    dz21 = z2-z1; %difference in z position between 2nd and 1st object in m
    dz31 = z3-z1; %difference in z position between 3rd and 1st object in m
    
    x3 = (x2-x1) / dz21*dz31 + x1; %x position of the 3rd object which was to determine in mm
    y3 = (y2-y1) / dz21*dz31 + y1; %y position of the 3rd object which was to determine in mm


varargout{1} = x3;
varargout{2} = y3;


