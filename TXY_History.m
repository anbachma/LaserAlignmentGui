function varargout = TXY_History(varargin)
% TXY_HISTORY MATLAB code for TXY_History.fig
%      TXY_HISTORY, by itself, creates a new TXY_HISTORY or raises the existing
%      singleton*.
%
%      H = TXY_HISTORY returns the handle to a new TXY_HISTORY or the handle to
%      the existing singleton*.
%
%      TXY_HISTORY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TXY_HISTORY.M with the given input arguments.
%
%      TXY_HISTORY('Property','Value',...) creates a new TXY_HISTORY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TXY_History_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TXY_History_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TXY_History

% Last Modified by GUIDE v2.5 11-Aug-2017 14:55:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TXY_History_OpeningFcn, ...
                   'gui_OutputFcn',  @TXY_History_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TXY_History is made visible.
function TXY_History_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TXY_History (see VARARGIN)

% Choose default command line output for TXY_History
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TXY_History wait for user response (see UIRESUME)
% uiwait(handles.fig_TXY);


% --- Outputs from this function are returned to the command line.
function varargout = TXY_History_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function ebox_Time_Min_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Time_Min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Time_Min as text
%        str2double(get(hObject,'String')) returns contents of ebox_Time_Min as a double
xlim1 = xlim(handles.ax_Entrance_TX);
xlim2 = xlim(handles.ax_Exit_TX);

t_lim(1) = min([xlim1(1) xlim2(1)]);
t_lim(2) = max([xlim1(2) xlim2(2)]);

val = str2double(handles.ebox_Time_Min.String);
val = val(1);
if isnan(val) | isinf(val) | val>t_lim(2)
    handles.ebox_Time_Min.String = num2str(t_lim(1));
    return
end
handles.ebox_Time_Min.String = num2str(val);
xlim(handles.ax_Entrance_TX, [val t_lim(2)]);
xlim(handles.ax_Exit_TX, [val t_lim(2)]);
xlim(handles.ax_Entrance_TY, [val t_lim(2)]);
xlim(handles.ax_Exit_TY, [val t_lim(2)]);


function ebox_Time_Max_Callback(hObject, eventdata, handles)
% hObject    handle to ebox_Time_Max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebox_Time_Max as text
%        str2double(get(hObject,'String')) returns contents of ebox_Time_Max as a double
xlim1 = xlim(handles.ax_Entrance_TX);
xlim2 = xlim(handles.ax_Exit_TX);

t_lim(1) = min([xlim1(1) xlim2(1)]);
t_lim(2) = max([xlim1(2) xlim2(2)]);

val = str2double(handles.ebox_Time_Max.String);
val = val(1);
if isnan(val) | isinf(val) | val<t_lim(1)
    handles.ebox_Time_Max.String = num2str(t_lim(2));
    return
end
handles.ebox_Time_Max.String = num2str(val);
xlim(handles.ax_Entrance_TX, [t_lim(1) val]);
xlim(handles.ax_Exit_TX, [t_lim(1) val]);
xlim(handles.ax_Entrance_TY, [t_lim(1) val]);
xlim(handles.ax_Exit_TY, [t_lim(1) val]);

% --- Executes during object creation, after setting all properties.
function ebox_Time_Min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Time_Min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function ebox_Time_Max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebox_Time_Max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
